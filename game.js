var buttonColors = ["red", "blue", "green", "yellow"];
var gamePattern = []; // sequence of colors that make up memory sequence
var patternIdx = 0; // index to game pattern array
var currentLevel = 1; // current level user is on
var hasClickListener = false; // keeps the program from adding more than one click listener to the buttons
var correctPattern = false; // whether or not the whole pattern is correct
var gameInProgress = false; // whether game is in progress or not
var endGame = false; // whether the game has ended

$(document).on("keydown", function() {
  if (!gameInProgress) {
    if (gamePattern.length !== 0) {
      // empty gamePattern
      gamePattern.length = 0;
      patternIdx = 0;
    }

    // reset game to starting values
    currentLevel = 1;
    endGame = false;
    gameInProgress = true;
    startGame();
  }
});

function startGame() {

  $("h1").text("Level " + currentLevel);

  // wait before giving user the first color of the game
  setTimeout(function() {
    // this is here b/c it prevents user from losing before ever starting a game
    nextSequence();
  }, 100);

  // add click listener
  if (!hasClickListener) {
    $(".btn").on("click", function(event) {
      hasClickListener = true;
      var currentClickedColor = event.currentTarget.id;


      // check if correct button was pressed
      endGame = !checkClick(currentClickedColor);


      // click animation and sound
      clickAnimation(currentClickedColor);

      if (endGame) {

        // wrong answer sound and animation
        wrongAnswerAnimation();

        // change h1 html to ask user to restart
        $("h1").text("Game Over, Press Any Key to Restart");

        // set gameInProgress to false to allow game to be restarted
        gameInProgress = false;
      }

      // if yes, continue with nextSequence (does this need to be an else if?)
      else if (correctPattern) {
        // reset correctPattern, patternIdx; add new color to sequence
        correctPattern = false;
        patternIdx = 0;

        currentLevel++;
        $("h1").text("Level " + currentLevel);

        // wait before giving user the next color in the sequence
        setTimeout(function() {
          nextSequence();
        }, 1000);
      }

    });
  }
}

function checkClick(currentClickedColor) {
  // if current button click matches color of most recent addition to pattern, you're click was correct
  if (currentClickedColor == gamePattern[patternIdx++]) {
    // if you're at the end of the pattern, the whole pattern is correct
    if (patternIdx === gamePattern.length) {
      // entire pattern is correct
      correctPattern = true;
    }
    return true;
  } else {
    // wrong answer
    return false;
  }
}

function nextSequence() {
  var newValue = Math.floor((Math.random() * 4));

  var newColor = buttonColors[newValue];

  newSequenceAnimation(newColor);

  gamePattern.push(newColor);
}

function whichSound(color) {

  var audio;

  // button sounds
  switch (color) {
    case "red":
      audio = new Audio('sounds/red.mp3');
      break;
    case "blue":
      audio = new Audio('sounds/blue.mp3');
      break;
    case "green":
      audio = new Audio('sounds/green.mp3');
      break;
    case "yellow":
      audio = new Audio('sounds/yellow.mp3');
      break;
  }
  return audio;
}

function newSequenceAnimation(newColor) {
  var audio = whichSound(newColor);
  audio.play();

  // sequence flash
  $("#" + newColor).fadeOut(50).fadeIn(50);
}

function clickAnimation(newColor) {
  var buttonAudio = whichSound(newColor);
  buttonAudio.play();

  // click flash
  $("#" + newColor).addClass("pressed");

  setTimeout(function() {
    $("#" + newColor).removeClass("pressed");
  }, 100);
}

function wrongAnswerAnimation() {

  var audio = new Audio('sounds/wrong.mp3');
  audio.play();

  // wrong answer flash animation
  $("body").addClass("game-over");
  setTimeout(function() {
    $("body").removeClass("game-over");
  }, 100);
}
